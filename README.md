# Project **Yokai-No-Mori**

## Credits:
-   Jacques Leslie-Anne
-   Piranda Nathan

## Branches :

- Use `git checkout 1-IA` to access the Java and Prolog-based project files.
- Use `git checkout 2-Communication` to access the server and client C files.

*Université de Franche-Comté - M1 Informatique ISL 2018/2019*
